//
//  ViewController.swift
//  Minhas Musicas
//
//  Created by COTEMIG on 18/08/22.
//

import UIKit

struct musica {
    let nomeMusica:String
    let nomealbum:String
    let nomeCantor:String
    let nomeImagemPequena:String
    let nomeImagemGrande:String
    
}
class ViewController: UIViewController , UITableViewDataSource, UITableViewDelegate {
    
    var listademusica: [musica] = []
    
    @IBOutlet weak var tableview: UITableView!
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listademusica.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! Mycell
        let muisca = self.listademusica[indexPath.row]
        
        cell.musica.text = muisca.nomeMusica
        cell.album.text = muisca.nomealbum
        cell.cantor.text = muisca.nomeCantor
        cell.capa.image = UIImage(named: muisca.nomeImagemPequena)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)  {
        self.performSegue(withIdentifier: "abrirDetalhe", sender: indexPath.row)
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender:Any?){
        let detalhesViewController = segue.destination as! DetalheMusicaViewController
        let indice = sender as! Int
        let musica = self.listademusica[indice]
        
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        self.tableview.dataSource = self
        self.tableview.delegate = self
        
        self.listademusica.append(musica(nomeMusica: "Pontos Caredais", nomealbum: "Álbum vivo", nomeCantor: "Alceu Valença", nomeImagemPequena: "Capa_Alceu_pequeno", nomeImagemGrande: "Capa_Alceu_Grande"))
        
        self.listademusica.append(musica(nomeMusica: "Menor Abandono", nomealbum: "Álbum patota de cosme", nomeCantor: "Zeca Pagodinho", nomeImagemPequena: "Capa_Zeca_Pequeno", nomeImagemGrande: "Capa_Zeca_Grande"))
        
        self.listademusica.append(musica(nomeMusica: "Trio ao Álvaro", nomealbum: "Álbum Adoniran Barbosa e Convidados", nomeCantor: "Adoniran Barbosa", nomeImagemPequena: "Capa_Adoniran_Pequeno", nomeImagemGrande: "Capa_Adoniran_grande"))
        
        
        
        
       
    
        tableview.dataSource = self
    

}

    }
